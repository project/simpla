<?php
function phptemplate_wrapperClass($vars) {
	if ($vars['sidebar_left'] && $vars['sidebar_right']) {
		$class = "mainContentSidebarBoth";
	} elseif ($vars['sidebar_left']) {
		$class = "mainContentSidebarLeft";
	} elseif ($vars['sidebar_right']) {
		$class = "mainContentSidebarRight";
	} else {
		$class = "mainContent";
	}
	return $class;
}

function _phptemplate_variables($hook, $vars) {
  switch ($hook) {
    case 'page':
      if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == '') {
        $vars['content_is_node'] = TRUE;
      }
	    if (module_exists('color')) {
	      _color_page_alter($vars);
	    }
    break;
  }
	$vars['wrapperClass'] = phptemplate_wrapperClass($vars);
	
  return $vars;
}


?>