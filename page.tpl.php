<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>

<!--[if IE]>
<style type="text/css"> 
/* place css fixes for all versions of IE in this conditional comment */
.twoColElsRtHdr #sidebar1 { padding-top: 30px; }
.twoColElsRtHdr #mainContent { zoom: 1; padding-top: 15px; }
/* the above proprietary zoom property gives IE the hasLayout it needs to avoid several bugs */
</style>
<![endif]-->
</head>

<body class="bd">
  <div id="header"><?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
	  <div id="menu"><?php if ($primary_links) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'primary_links')) ?><?php } ?></div></div>

  <?php if($breadcrumb) { ?>
    <div id="breadcrumb">
      <div class="wrapper">
      <?php print $breadcrumb; ?>
      </div>
    </div>
  <?php } ?>
  <div class="topShadow"></div>

<div id="container" class="clear-block">

  <div id="contentArea">
    


  <?php if ($sidebar_left) { ?>
    <div id="sidebarLeft">
      <?php print $sidebar_left ?>
    </div>
  <?php } ?>    
  <?php if ($sidebar_right) { ?>
    <div id="sidebarRight">
      <?php print $sidebar_right ?>
    </div>
  <?php } ?>
  <div id="<?php print $wrapperClass ?>" class="">

        <h1 id="title"><?php print $title ?></h1>

        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content ?>
        <br class="clearfloat" />
  </div>
  <br class="clearfloat" />

  <?php if (($footer_message > 0) || ($feed_icons) || ($secondary_links)) { ?>
  <div id="footer">
    <div class="wrapper">
  <?php
    if ($secondary_links) { 
      print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist'));
    }
    if (($footer_message) || ($feed_icons)) {
      echo "<p>";
      if ($footer_message) print $footer_message;
      if ($feed_icons) print $feed_icons;
      echo "</p>";
    } 
  ?>
      </div>
  </div>
  <?php } ?>
</div>
<?php print $closure ?>
</body>
</html>